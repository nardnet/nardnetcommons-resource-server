# NardnetCommons-resource-server



## Getting started

## Project status
This project is used to send FAKE, SANDBOX, SMTP emails..

Very important to use of the dependency put in POM like this:

```
      <dependency>
			<groupId>com.nardnet</groupId>
			<artifactId>NardnetCommons-resource-server</artifactId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency>
		
	 <repositories>
		<repository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</repository>
	 </repositories>

	<distributionManagement>
		<repository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</repository>

		<snapshotRepository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</snapshotRepository>
	</distributionManagement>
		
		
```

## Application.properties

server.port=8082
spring.security.oauth2.resourceserver.jwt.jwk-set-uri=http://YOURURL:8081/.well-known/jwks.json

spring.security.oauth2.resourceserver.opaquetoken.introspection-uri=http://YOURURL:8081/oauth/check_token

spring.security.oauth2.resourceserver.opaquetoken.client-id=[X]

spring.security.oauth2.resourceserver.opaquetoken.client-secret=[X]

logging.level.org.springframework=[DEBUG]


