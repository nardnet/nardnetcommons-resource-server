package com.nardnet.commons.security.infrastructure.audit;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Service;

import com.nardnet.commons.security.infrastructure.AuthSecutiry;

@Service
public class AuditingService implements AuditorAware<String>{
	
	@Autowired
	private AuthSecutiry authSecurity;

	@Override
	public Optional<String> getCurrentAuditor() {	
		return Optional.ofNullable(authSecurity.getFullName());
	}

}
