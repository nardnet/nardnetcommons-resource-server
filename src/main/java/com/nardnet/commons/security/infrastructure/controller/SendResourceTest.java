package com.nardnet.commons.security.infrastructure.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SendResourceTest {


	@PreAuthorize("isAuthenticated()")
	@GetMapping("/data")
	public ResponseEntity<HttpStatus> getData() {
		
		System.out.println("Entrou");

		
		return ResponseEntity.ok(HttpStatus.OK);
	}

}
