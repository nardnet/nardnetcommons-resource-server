package com.nardnet.commons.security.infrastructure.config;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

@Configuration
@EnableWebSecurity
public class ResourceServerConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(requests -> requests                              
                                .antMatchers("/swagger-ui/**").permitAll()
                                .antMatchers("/v3/api-docs/**").permitAll()
                                .antMatchers("/swagger-ui.html").permitAll()
                                .antMatchers("/oauth/**").authenticated()                           
                                .anyRequest().authenticated()
                )
                .formLogin(login -> login.loginPage("/login")) 
                .csrf(csrf -> csrf.disable()) 
                .oauth2ResourceServer(server -> server.jwt().jwtAuthenticationConverter(jwtAuthenticationConverter())); // Configuração do servidor OAuth2
    }

	private JwtAuthenticationConverter jwtAuthenticationConverter() {

		var jwtAuthenticationConverter = new JwtAuthenticationConverter();
		jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(jwt -> {
			var authorities = jwt.getClaimAsStringList("authorities");

			if (Objects.isNull(authorities)) {
				authorities = Collections.emptyList();
			}
			
			var scopesAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
			Collection<GrantedAuthority> grantedAuthories = scopesAuthoritiesConverter.convert(jwt);
			
			grantedAuthories.addAll(authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

			return grantedAuthories;
		});

		return jwtAuthenticationConverter;

	}

}
