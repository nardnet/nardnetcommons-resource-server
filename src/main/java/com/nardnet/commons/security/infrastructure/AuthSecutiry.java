package com.nardnet.commons.security.infrastructure;

import java.util.UUID;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

@Component
public class AuthSecutiry {
	
	public Authentication getAuthentication(){
		return SecurityContextHolder.getContext().getAuthentication();
	}
	
	public UUID getUserId() {
		
		Jwt jwt = (Jwt) getAuthentication().getPrincipal();		
		return jwt.getClaim("user_id");
	}
	
	public String getFullName() {
		
		Jwt jwt = (Jwt) getAuthentication().getPrincipal();		
		return jwt.getClaim("full_name");
	}

}
